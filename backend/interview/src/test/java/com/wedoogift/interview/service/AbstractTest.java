package com.wedoogift.interview.service;

import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.helper.DataHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;

public class AbstractTest {
    @BeforeEach
    public void before() throws IOException {
        var inputDTO = DataHelper.readInputFile();
        FakeDatabase.initDatabase(inputDTO);
    }

    @AfterEach
    public void after() {
        FakeDatabase.dropAll();
    }

}
