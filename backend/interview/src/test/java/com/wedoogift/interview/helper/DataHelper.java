package com.wedoogift.interview.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.interview.model.Distribution;
import com.wedoogift.interview.model.InputDTO;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DataHelper {
    private static final Logger LOGGER = Logger.getLogger(DataHelper.class);

    public static InputDTO readInputFile() throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = loader.getResourceAsStream("Level2/data/input.json");

        var objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(inputStream, InputDTO.class);
        } catch (IOException e) {
            LOGGER.error("Could not read init file", e);
            throw e;
        }
    }

    public static Distribution getDistribution() {
        Date startDate = Timestamp.valueOf(LocalDateTime.now().minusMonths(6));
        Date endDate = Timestamp.valueOf(LocalDateTime.now().plusMonths(6).minusDays(1));

        var distribution = new Distribution();
        distribution.setId(1);
        distribution.setWalletId(1);
        distribution.setAmount(100);
        distribution.setStartDate(startDate);
        distribution.setEndDate(endDate);
        distribution.setCompanyId(1);
        distribution.setUserId(1);

        return distribution;
    }

    public static Distribution getVoucher() {
        Date startDate = Date.from(LocalDate.parse("2021-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date endDate = Date.from(LocalDate.parse("2022-02-28").atStartOfDay(ZoneId.systemDefault()).toInstant());

        var distribution = new Distribution();
        distribution.setId(1);
        distribution.setWalletId(1);
        distribution.setAmount(100);
        distribution.setStartDate(startDate);
        distribution.setEndDate(endDate);
        distribution.setCompanyId(1);
        distribution.setUserId(1);

        return distribution;
    }
}
