package com.wedoogift.interview.service;

import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.helper.DataHelper;
import com.wedoogift.interview.model.Company;
import com.wedoogift.interview.model.User;
import com.wedoogift.interview.model.Wallet;
import com.wedoogift.interview.model.exception.EndowmentException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

public class EndowmentServiceTest extends AbstractTest {
    private GiftCardService giftCardService = new GiftCardService();
    private VoucherService voucherService = new VoucherService();

    @Test
    // we check the balance has been increased for the right wallet
    public void shouldAddGiftCard() throws EndowmentException {
        // given
        var distribution = DataHelper.getDistribution();
        ArrayList<Wallet> wallets = FakeDatabase.getUserById(distribution.getUserId()).get().getBalance();
        int balance1 = 0;
        int balance2 = 0;
        for (Wallet wallet : wallets) {
            switch (wallet.getId()) {
                case 1:
                    balance1 = wallet.getAmount();
                    break;
                case 2:
                    balance2 = wallet.getAmount();
                    break;
                default:
                    break;
            }
        }
        var companyBalance = FakeDatabase.getCompanyAccountById(distribution.getCompanyId()).get().getBalance();

        // when
        giftCardService.distributeEndowment(distribution);

        User updatedUser = FakeDatabase.getUserById(distribution.getUserId()).get();
        Company updatedCompany = FakeDatabase.getCompanyAccountById(distribution.getCompanyId()).get();

        // then
        for (Wallet wallet : updatedUser.getBalance()) {
            if (wallet.getId() == distribution.getWalletId()) {
                Assertions.assertEquals(balance1 + distribution.getAmount(), updatedUser.getBalanceFromWallet(wallet.getId()));
            } else {
                Assertions.assertEquals(balance2, updatedUser.getBalanceFromWallet(wallet.getId()));
            }
        }
        Assertions.assertEquals(companyBalance - distribution.getAmount(), updatedCompany.getBalance());
        Assertions.assertEquals(1, FakeDatabase.getDistributionList().size());
    }

    @Test
    public void shouldAddMealVoucher() throws EndowmentException {
        // given
        var distribution = DataHelper.getVoucher();
        ArrayList<Wallet> wallets = FakeDatabase.getUserById(distribution.getUserId()).get().getBalance();
        int balance1 = 0;
        int balance2 = 0;
        for (Wallet wallet : wallets) {
            switch (wallet.getId()) {
                case 1:
                    balance1 = wallet.getAmount();
                    break;
                case 2:
                    balance2 = wallet.getAmount();
                    break;
                default:
                    break;
            }
        }
        var companyBalance = FakeDatabase.getCompanyAccountById(distribution.getCompanyId()).get().getBalance();

        // when
        voucherService.distributeEndowment(distribution);

        User updatedUser = FakeDatabase.getUserById(distribution.getUserId()).get();
        Company updatedCompany = FakeDatabase.getCompanyAccountById(distribution.getCompanyId()).get();

        // then
        for (Wallet wallet : updatedUser.getBalance()) {
            if (wallet.getId() == distribution.getWalletId()) {
                Assertions.assertEquals(balance1 + distribution.getAmount(), updatedUser.getBalanceFromWallet(wallet.getId()));
            } else {
                Assertions.assertEquals(balance2, updatedUser.getBalanceFromWallet(wallet.getId()));
            }
        }
        Assertions.assertEquals(companyBalance - distribution.getAmount(), updatedCompany.getBalance());
        Assertions.assertEquals(1, FakeDatabase.getDistributionList().size());
    }

    @Test
    public void shouldFailEndowment_whenCompanyAccountTooLow() {
        // given
        var distribution = DataHelper.getDistribution();
        distribution.setAmount(10000);

        // when
        // then
        Assertions.assertThrows(EndowmentException.class, () -> giftCardService.distributeEndowment(distribution),
            "Expected distributeEndowment to fail");

    }

    @Test
    public void shouldFailEndowment_whenDistributionInvalid() {
        // given
        var distribution = DataHelper.getDistribution();
        distribution.setStartDate(null);
        distribution.setEndDate(null);

        // when
        // then
        Assertions.assertThrows(EndowmentException.class, () -> giftCardService.distributeEndowment(distribution),
            "Expected distributeGiftCard to fail");

    }

    @Test
    public void shouldFailEndowment_whenDistributionDateTooWide() {
        // given
        var distribution = DataHelper.getDistribution();
        distribution.setStartDate(new Date());
        Date startDate = Timestamp.valueOf(LocalDateTime.now().plusDays(400));
        distribution.setStartDate(startDate);

        // when
        // then
        Assertions.assertThrows(EndowmentException.class, () -> giftCardService.distributeEndowment(distribution),
            "Expected distributeGiftCard to fail");

    }


    @Test
    public void shouldCalculateUserBalance() throws EndowmentException {

        // given
        var distribution = DataHelper.getDistribution();
        giftCardService.distributeEndowment(distribution);

        // when
        int userbalance = giftCardService.calculateUserBalance(1, 1);

        // then
        Assertions.assertEquals(100, userbalance);
    }
}
