package com.wedoogift.interview.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserServiceTest extends AbstractTest {
    @Test
    public void shouldGetUser() {
        // given
        var userService = new UserService();

        // when
        var user = userService.getUserById(1);

        // then
        Assertions.assertTrue(user.isPresent());
        Assertions.assertEquals(1, user.get().getId());
    }

    @Test
    public void shouldNotGetUser_WhenIdDoesNotExist() {
        // given
        var userService = new UserService();

        // when
        var user = userService.getUserById(5);

        // then
        Assertions.assertTrue(user.isEmpty());
    }
}
