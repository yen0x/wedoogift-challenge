package com.wedoogift.interview;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.interview.helper.DataHelper;
import com.wedoogift.interview.service.AbstractTest;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class EndowmentResourceTest extends AbstractTest {

    @Test
    public void testEndowmendPost() throws JsonProcessingException {
        var distribution = DataHelper.getDistribution();
        String requestBody = new ObjectMapper().writeValueAsString(distribution);
        given().auth().preemptive().basic("admin", "pwd")
            .header("Content-type", "application/json")
            .and()
            .body(requestBody)
            .when()
            .post("/endowment")
            .then()
            .statusCode(201)
            .body(is(requestBody));
    }

    @Test
    public void testEndowmendPost_400CompanyNotFound() throws JsonProcessingException {
        var distribution = DataHelper.getDistribution();
        distribution.setCompanyId(10);
        String requestBody = new ObjectMapper().writeValueAsString(distribution);
        given().auth().preemptive().basic("admin", "pwd")
            .header("Content-type", "application/json")
            .and()
            .body(requestBody)
            .when()
            .post("/endowment")
            .then()
            .statusCode(400)
            .body(is("Company not found, cannot add Gift Card for distribution 1"));
    }

}