package com.wedoogift.interview;

import com.wedoogift.interview.service.AbstractTest;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class UserResourceTest extends AbstractTest {

    @Test
    public void testGetUserBalance() {
        given().auth().preemptive().basic("admin", "pwd")
            .when()
            .get("/user/1/wallet/1")
            .then()
            .statusCode(200)
            .body(is("0"));
    }

    @Test
    public void testGetUserBalance_400onUserNotFound() {
        given().auth().preemptive().basic("admin", "pwd")
            .when()
            .get("/user/10/wallet/1")
            .then()
            .statusCode(400)
            .body(is("User not found"));
    }
}
