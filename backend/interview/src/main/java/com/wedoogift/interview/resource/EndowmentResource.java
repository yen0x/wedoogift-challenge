package com.wedoogift.interview.resource;

import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.model.Distribution;
import com.wedoogift.interview.model.WalletDTO;
import com.wedoogift.interview.model.exception.EndowmentException;
import com.wedoogift.interview.service.GiftCardService;
import com.wedoogift.interview.service.VoucherService;
import org.jboss.logging.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.core.Response;
import java.util.List;

@RestController
@RequestMapping("/endowment")
public class EndowmentResource {

    private static final Logger LOGGER = Logger.getLogger(EndowmentResource.class);

    @Inject
    GiftCardService giftCardService;
    @Inject
    VoucherService voucherService;

    @Secured("admin")
    @PostMapping
    public Response addNewDistribution(Distribution distribution) {
        List<WalletDTO> walletTypes = FakeDatabase.getWalletTypes();
        int giftCardId = 0;
        int mealVoucherid = 0;
        for (WalletDTO walletType : walletTypes) {
            if (walletType.type.equals("FOOD")) {
                mealVoucherid = walletType.id;
            } else if (walletType.type.equals("GIFT")) {
                giftCardId = walletType.id;
            }
        }
        try {
            if (giftCardId == distribution.getWalletId())
                giftCardService.distributeEndowment(distribution);
            else if (mealVoucherid == distribution.getWalletId())
                voucherService.distributeEndowment(distribution);
            else
                throw new EndowmentException("Wrong wallet for distribution " + distribution.getWalletId());
        } catch (EndowmentException e) {
            LOGGER.warn(e.getMessage(), e);
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        return Response.status(Response.Status.CREATED).entity(distribution).build();
    }
}