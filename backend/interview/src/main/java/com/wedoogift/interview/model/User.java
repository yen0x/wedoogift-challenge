package com.wedoogift.interview.model;

import java.util.ArrayList;

public class User {
    private int id;
    private ArrayList<Wallet> balance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addAmount(int walletId, int amount) {
        for (Wallet wallet : balance) {
            if (wallet.getId() == walletId) {
                wallet.setAmount(wallet.getAmount() + amount);
                return;
            }
        }
        // if we didn't find the wallet, we initialize it
        balance.add(new Wallet(walletId, amount));
    }

    public ArrayList<Wallet> getBalance() {
        return balance;
    }

    public void setBalance(ArrayList<Wallet> balance) {
        this.balance = balance;
    }

    // makes it easier to test the wallet values
    public int getBalanceFromWallet(int walletId) {
        for (Wallet wallet : balance) {
            if (wallet.getId() == walletId) {
                return wallet.getAmount();
            }
        }
        return 0;
    }
}
