package com.wedoogift.interview;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.model.InputDTO;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.io.InputStream;

@QuarkusMain
public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class);

    public static void main(String... args) {
        Quarkus.run(EndowmentApp.class, args);
    }

    public static class EndowmentApp implements QuarkusApplication {

        @Override
        public int run(String... args) throws Exception {

            var inputDTO = readInputFile();
            FakeDatabase.initDatabase(inputDTO);

            LOGGER.info("startup logic done");

            Quarkus.waitForExit();
            return 0;
        }
    }

    private static InputDTO readInputFile() throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = loader.getResourceAsStream("Level2/data/input.json");

        var objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(inputStream, InputDTO.class);
        } catch (IOException e) {
            LOGGER.error("Could not read init file", e);
            throw e;
        }
    }
}
