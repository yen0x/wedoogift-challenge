package com.wedoogift.interview.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * This serves as a DTO for reading/writing input/output files
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InputDTO {
    public List<WalletDTO> wallets;
    public List<Company> companies;
    public List<User> users;
    public List<Distribution> distributions;
}
