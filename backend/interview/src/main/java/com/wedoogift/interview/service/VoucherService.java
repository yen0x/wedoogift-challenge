package com.wedoogift.interview.service;

import com.wedoogift.interview.model.Distribution;
import com.wedoogift.interview.model.exception.EndowmentException;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;

@ApplicationScoped
public class VoucherService extends EndowmentService {

    // checks the meal voucher duration
    public void checkEndowmentValidity(Distribution distribution) throws EndowmentException {
        if (distribution.getStartDate() == null || distribution.getEndDate() == null) {
            throw new EndowmentException("Meal voucher invalid, dates required");
        }

        LocalDate startDate = distribution.getStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = distribution.getEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int lastFebDay = endDate.isLeapYear() ? 29 : 28;
        // verifying that endDate is on the last day of february of startDate + 1
        if (!(startDate.getYear() + 1 == endDate.getYear() && endDate.getMonth() == Month.FEBRUARY && endDate.getDayOfMonth() == lastFebDay)) {
            throw new EndowmentException("Meal voucher invalid, invalid duration");
        }


    }
}
