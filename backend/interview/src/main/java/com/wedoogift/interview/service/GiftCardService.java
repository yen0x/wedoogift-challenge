package com.wedoogift.interview.service;

import com.wedoogift.interview.model.Distribution;
import com.wedoogift.interview.model.exception.EndowmentException;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.time.ZoneId;

import static java.time.temporal.ChronoUnit.DAYS;

@ApplicationScoped
public class GiftCardService extends EndowmentService {
    // we check gift card duration is set and allowed
    public void checkEndowmentValidity(Distribution distribution) throws EndowmentException {
        if (distribution.getStartDate() == null || distribution.getEndDate() == null) {
            throw new EndowmentException("Gift card invalid, dates required");
        }

        LocalDate startDate = distribution.getStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = distribution.getEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        long daysBetween = DAYS.between(startDate, endDate) + 1;

        if (daysBetween != 365) {
            throw new EndowmentException("Gift card invalid, invalid duration");
        }
    }

}
