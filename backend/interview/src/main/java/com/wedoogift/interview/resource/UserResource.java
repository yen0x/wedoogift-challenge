package com.wedoogift.interview.resource;

import com.wedoogift.interview.model.User;
import com.wedoogift.interview.service.GiftCardService;
import com.wedoogift.interview.service.UserService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.Optional;

@RestController
@RequestMapping("user")
public class UserResource {

    @Inject
    UserService userService;

    @Inject
    GiftCardService endowmentService;

    @Secured("admin")
    @RequestMapping(value = "{userId}/wallet/{walletId}", method = RequestMethod.GET, produces = "text/plain")
    public Response getUserBalance(@PathVariable("userId") int userId, @PathVariable("walletId") int walletId) {
        Optional<User> userById = userService.getUserById(userId);
        if (userById.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).entity("User not found").build();
        }

        int balance = endowmentService.calculateUserBalance(userId, walletId);

        return Response.ok().entity(balance).build();
    }
}
