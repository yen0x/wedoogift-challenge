package com.wedoogift.interview.service;

import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.model.User;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class UserService {
    public Optional<User> getUserById(int id) {
        return FakeDatabase.getUserById(id);
    }
}
