package com.wedoogift.interview.service;

import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.model.Company;
import com.wedoogift.interview.model.Distribution;
import com.wedoogift.interview.model.WalletDTO;
import com.wedoogift.interview.model.exception.EndowmentException;

import java.util.List;

// We now use implementations of this service for vouchers and gift cards so that each service implements its own validity checks
public abstract class EndowmentService {

    // we check the validity of the gift card then update the balances of the company and the user
    public void distributeEndowment(Distribution distribution) throws EndowmentException {
        checkEndowmentType(distribution);
        checkEndowmentValidity(distribution);
        var company = checkCompanyAndBalance(distribution);
        // V2 I disabled this check because I noticed you don't take it into account (distribution4 from the example
        // should not be applied otherwise
        //if (distribution.getStartDate().before(new Date()) && distribution.getEndDate().after(new Date())) {
        FakeDatabase.updateUserBalance(distribution.getUserId(), distribution.getWalletId(), distribution.getAmount());
        FakeDatabase.updateCompanyBalance(company.getId(), -1 * distribution.getAmount());
        //}
        FakeDatabase.addDistribution(distribution);
    }

    public abstract void checkEndowmentValidity(Distribution distribution) throws EndowmentException;

    // Not much done here as the algorithm is run by the db (or would be in a real life application)
    public int calculateUserBalance(int userId, int walletId) {
        return FakeDatabase.calculateUserBalance(userId, walletId);
    }


    protected Company checkCompanyAndBalance(Distribution distribution) throws EndowmentException {
        var optionalCompany = FakeDatabase.getCompanyAccountById(distribution.getCompanyId());

        // checking that we found a company and shortening the optional for further use
        var company =
            optionalCompany.orElseThrow(() -> new EndowmentException("Company not found, cannot add Gift Card for distribution " + distribution.getId()));


        if (company.getBalance() < distribution.getAmount())
            throw new EndowmentException(String.format("Insufficient funds at company %d, for distribution %d", company.getId(), distribution.getId()));

        return company;
    }

    // checks that the wallet type exists in db
    private void checkEndowmentType(Distribution distribution) throws EndowmentException {
        List<WalletDTO> walletTypes = FakeDatabase.getWalletTypes();
        for (WalletDTO walletType : walletTypes) {
            if (distribution.getWalletId() == walletType.id) {
                return;
            }
        }
        throw new EndowmentException("Wallet type does not exist");
    }
}
