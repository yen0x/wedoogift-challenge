package com.wedoogift.interview.dao;

import com.wedoogift.interview.model.*;

import java.util.*;

/**
 * For convenience I decided to just store everything in map objects
 * And use them as if they were regular SQL tables
 * In a real world application this would obviously be split into UserDAO, CompanyDAO and DistributionDAO
 * <p>
 * This class contains operations on the fake in-memory tables
 **/
public class FakeDatabase {

    // Using maps to retrieve object by id quickly
    // Map stores users as id => user
    private static final Map<Integer, User> inMemoryUserTable = new HashMap<>();
    // Map stores companies as id => company
    private static final Map<Integer, Company> inMemoryCompanyTable = new HashMap<>();
    // Map stores endowments as id => distribution
    private static final Map<Integer, Distribution> inMemoryDistributionTable = new HashMap<>();

    private static List<WalletDTO> walletTypes;

    public static void initDatabase(InputDTO inputDTO) {
        inputDTO.users.stream().forEach(u -> inMemoryUserTable.put(u.getId(), u));
        inputDTO.companies.stream().forEach(c -> inMemoryCompanyTable.put(c.getId(), c));
        walletTypes = inputDTO.wallets;
    }

    // --User--
    //SQL: SELECT * FROM user WHERE id = $1;
    public static Optional<User> getUserById(int userId) {
        var user = inMemoryUserTable.get(userId);
        return user == null ? Optional.empty() : Optional.of(user);
    }

    //SQL: UPDATE wallet SET balance=balance+amount WHERE id = $1 and user_id = $2;
    public static void updateUserBalance(int userId, int walletid, int amount) {
        var user = inMemoryUserTable.get(userId);
        if (user == null) return;

        user.addAmount(walletid, amount);
        inMemoryUserTable.put(userId, user);
    }

    //SQL: SELECT sum(amount) FROM distribution WHERE start_date<now() AND end_date>now() AND user_id = $1 AND walletId = $2;
    // checks each distribution with dates, userId and walletId and sums up to find the balance of the user's wallet
    public static int calculateUserBalance(int userId, int walletId) {
        int balance = 0;
        List<Distribution> distributions = getDistributionList();
        for (Distribution distribution : distributions) {
            if (userId == distribution.getUserId() && walletId == distribution.getWalletId()
                && new Date().before(distribution.getEndDate())
                && new Date().after(distribution.getStartDate())) {
                balance += distribution.getAmount();
            }
        }
        return balance;
    }

    //SQL: SELECT * FROM user;
    public static List<User> getUserList() {
        return new ArrayList<>(inMemoryUserTable.values());
    }

    // --Company--

    //SQL: SELECT * FROM company WHERE id = $1;
    public static Optional<Company> getCompanyAccountById(int companyId) {
        var company = inMemoryCompanyTable.get(companyId);
        return company == null ? Optional.empty() : Optional.of(company);
    }

    //SQL: UPDATE company SET balance=balance+amount WHERE id = $1;
    public static void updateCompanyBalance(int companyId, int amount) {
        var company = inMemoryCompanyTable.get(companyId);
        company.setBalance(company.getBalance() + amount);
        inMemoryCompanyTable.put(companyId, company);
    }

    //SQL: SELECT * FROM company;
    public static List<Company> getCompanyList() {
        return new ArrayList<>(inMemoryCompanyTable.values());
    }

    // --Distribution--
    //SQL: INSERT INTO distribution (id, amount, ...) VALUES ($1, $2, ...)
    public static void addDistribution(Distribution distribution) {
        inMemoryDistributionTable.put(distribution.getId(), distribution);
    }

    //SQL: SELECT * FROM distribution;
    public static List<Distribution> getDistributionList() {
        return new ArrayList<>(inMemoryDistributionTable.values());
    }

    // --Wallets--
    public static List<WalletDTO> getWalletTypes() {
        return walletTypes;
    }

    // --Tests--
    public static void dropAll() {
        inMemoryUserTable.clear();
        inMemoryCompanyTable.clear();
        inMemoryDistributionTable.clear();
    }
}
