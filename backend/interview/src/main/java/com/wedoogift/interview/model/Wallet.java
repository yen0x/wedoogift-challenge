package com.wedoogift.interview.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Wallet {
    @JsonProperty("wallet_id")
    private int id;
    private int amount;

    public Wallet() {
    }

    public Wallet(int id, int amount) {
        this.id = id;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
