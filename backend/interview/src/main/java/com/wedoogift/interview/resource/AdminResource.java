package com.wedoogift.interview.resource;

import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.model.InputDTO;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is only useful to generate json from the fakedatabase
 */
@RestController
@RequestMapping("admin")
public class AdminResource {

    @Secured("admin")
    @GetMapping
    @RequestMapping("db")
    public InputDTO dumpDB() {
        InputDTO result = new InputDTO();
        result.users = FakeDatabase.getUserList();
        result.companies = FakeDatabase.getCompanyList();
        result.distributions = FakeDatabase.getDistributionList();
        return result;
    }
}
