# Solution
## Level1
I have setup a quarkus application to answer the two questions of this level. This will also allow me to answer the rest of the challenge faster once everything is setup. Also Quarkus is a small microservice framework that allows for a quick and efficient setup and easy packaging of java applications, which could be handy in that kind of challenges. You will find all the code in the "interview" directory.

My code is split into models to represent endowments, companies and users; services, to perform operations on the models; a fake database to store the data conveniently; and resources to allow for REST interactions with the app.
All tests are stored in `src/test/java`, I have built a few tests for resources and services.

### Requirements:
- java 11
- maven 3.6.2+

You can run the application with `java -jar Level1/quarkus-app/quarkus-run.jar` it will start the quarkus-app on port 8080 (or use `./mvnw compile quarkus:dev` from the root source) and then execute the curl calls suggested below. The application starts with the data from `Level1/data/input.json` loaded. If you just wanna check the functions in the service, the \*ServiceTest also permit to run the requested code only with the help of an IDE.
**Everytime you restart the app the database is flushed.**

- Implement the function to distribute gift cards to the user by the company if the company balance allows it:
The function is available in EndowmentService.distributeGiftCard and you can use it via the resource with a post call like this:
```
curl http://localhost:8080/endowment -H "Content-Type: application/json" -d '{
      "id": 1,
      "amount": 50,
      "start_date": "2020-09-16",
      "end_date": "2021-09-15",
      "company_id": 1,
      "user_id": 1
    }'

```

Change the parameters to your convenience. The EndowmentServiceTest class can test the code of the function EndowmentService.distributeGiftCard.


- Implement the function to calculate the user's balance:
*Note: In a real case, I'd check the expiration of gift cards with a cron and/or event-driven structure regularly as checking directly in the database might end up giving poor performances depending on the volume. Here I am just checking the validity of the dates provided and adding the amount to the user balance if the card is currently available*

```
curl http://localhost:8080/user/1
```

Change the user id in the url path as you wish.  The EndowmentServiceTest class can test the code of the function EndowmentService.calculateUserBalance.


**You can drop the state of the database in a file with:**
```
curl http://localhost:8080/admin/db > output.json
```

## Level2

You can run the application with `java -jar Level2/quarkus-app/quarkus-run.jar` it will start the quarkus-app on port 8080.

- Implement the function allowing the company to distribute meal vouchers:
You can find the new function in EndowmentService and VoucherService for the validity checks. I have now add a layer in services to differentiate checks between vouchers and gift cards.


- Use this function to generate the expected result in the output-expected.json file:

If you launch all these curls and then get the admin output you will have the expected json:

```
curl http://localhost:8080/endowment -H "Content-Type: application/json" -d '{
      "id": 1,
      "wallet_id": 1,
      "amount": 50,  
      "start_date": "2020-09-16",
      "end_date": "2021-09-15",
      "company_id": 1,
      "user_id": 1
    }'

curl http://localhost:8080/endowment -H "Content-Type: application/json" -d '{
      "id": 2,
      "wallet_id": 1,
      "amount": 100, 
      "start_date": "2020-08-01",
      "end_date": "2021-07-31",
      "company_id": 1,
      "user_id": 2
    }'

curl http://localhost:8080/endowment -H "Content-Type: application/json" -d '{
      "id": 3,
      "wallet_id": 1,
      "amount": 1000,
      "start_date": "2020-05-01",
      "end_date": "2021-04-30",
      "company_id": 2,
      "user_id": 3
    }'

curl http://localhost:8080/endowment -H "Content-Type: application/json" -d '{
      "id": 4,
      "wallet_id": 2,
      "amount": 250,
      "start_date": "2020-05-01",
      "end_date": "2021-02-28",
      "company_id": 1,
      "user_id": 1
    }'

curl http://localhost:8080/admin/db > output.json
```

## Level3

You can run the application with `java -jar Level3/quarkus-app/quarkus-run.jar` it will start the quarkus-app on port 8080.

- A REST controller for the functions implemented in level 1 and 2:

That's already done as I built everything with quarkus.

- Secure the endpoints using spring security. feel free to choose the security mechanism:

I have only added a simple basic auth with a hardcoded user and role. I also had to change annotations from jax-rs to spring-web in combination to spring-security.

You now have to add a user and password (admin/pwd) to send your requests, otherwise you'll get a 401 response status. Example

```
curl -u admin:pwd http://localhost:8080/admin/db -i

curl http://localhost:8080/admin/db -i #failing request
```

The *ResourceTest classes can showcase some tests of the requests as well.
