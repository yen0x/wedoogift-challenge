package com.wedoogift.interview.service;

import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.model.Distribution;
import com.wedoogift.interview.model.exception.EndowmentException;

import java.util.Date;

public class EndowmentService {

    public void distributeGiftCard(Distribution distribution) throws EndowmentException {

        if (new Date().after(distribution.getEndDate()))
            throw new EndowmentException("Distribution is expired");

        var optionalCompany = FakeDatabase.getCompanyAccountById(distribution.getCompanyId());

        // checking that we found a company and shortening the optional for further usage
        var company =
            optionalCompany.orElseThrow(() -> new EndowmentException("Company not found, cannot add Gift Card for distribution " + distribution.getId()));


        if (company.getBalance() < distribution.getAmount())
            throw new EndowmentException(String.format("Insufficient funds at company %d, for distribution %d", company.getId(), distribution.getId()));

        FakeDatabase.updateUserBalance(distribution.getUserId(), distribution.getAmount());
        FakeDatabase.updateCompanyBalance(company.getId(), -1*distribution.getAmount());
    }
}
