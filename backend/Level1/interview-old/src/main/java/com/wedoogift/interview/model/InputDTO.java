package com.wedoogift.interview.model;

import java.util.List;

public class InputDTO {
    public List<Company> companies;
    public List<User> users;
    public List<Distribution> distributions;
}
