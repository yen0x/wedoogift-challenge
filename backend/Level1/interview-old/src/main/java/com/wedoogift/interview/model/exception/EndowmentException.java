package com.wedoogift.interview.model.exception;

public class EndowmentException extends Exception {
    public EndowmentException(String message) {
        super(message);
    }
}
