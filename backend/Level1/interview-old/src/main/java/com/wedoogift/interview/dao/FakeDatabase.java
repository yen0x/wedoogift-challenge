package com.wedoogift.interview.dao;

import com.wedoogift.interview.model.Company;
import com.wedoogift.interview.model.InputDTO;
import com.wedoogift.interview.model.User;

import java.util.*;

/**
 * For this first level I am pretending the input file is the current database
 * I will implement an real in-memory database for the last level if necessary
 * <p>
 * This class contains operations on the fake in-memory tables
 **/
public class FakeDatabase {

    // Using maps to retrieve object by id quickly
    // Map stores users as id => user
    private static final Map<Integer, User> inMemoryUserTable = new HashMap<>();
    // Map stores companies as id => company
    private static final Map<Integer, Company> inMemoryCompanyTable = new HashMap<>();

    public static void initDatabase(InputDTO inputDTO) {
        inputDTO.users.stream().forEach(u -> inMemoryUserTable.put(u.getId(), u));
        inputDTO.companies.stream().forEach(c -> inMemoryCompanyTable.put(c.getId(), c));
    }

    // --User--

    //SQL: UPDATE user SET balance=balance+amount WHERE id = $1;
    public static void updateUserBalance(int userId, int amount) {
        var user = inMemoryUserTable.get(userId);
        if (user == null) return;

        user.addAmount(amount);
        inMemoryUserTable.put(userId, user);
    }

    //SQL: SELECT * FROM user;
    public static List<User> getUserList() {
        return new ArrayList<>(inMemoryUserTable.values());
    }

    // --Company--
    //SQL: SELECT * FROM company WHERE id = $1;
    public static Optional<Company> getCompanyAccountById(int companyId) {
        var company = inMemoryCompanyTable.get(companyId);
        return company == null ? Optional.empty() : Optional.of(company);
    }

    //SQL: UPDATE company SET balance=balance+amount WHERE id = $1;
    public static void updateCompanyBalance(int companyId, int amount) {
        var company = inMemoryCompanyTable.get(companyId);
        company.setBalance(company.getBalance() + amount);
        inMemoryCompanyTable.put(companyId, company);
    }

    //SQL: SELECT * FROM company;
    public static List<Company> getCompanyList() {
        return new ArrayList<>(inMemoryCompanyTable.values());
    }
}
