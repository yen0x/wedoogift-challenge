package com.wedoogift.interview.model;

public class User {
    private int id;
    private int balance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void addAmount(int amount) {
        this.balance += amount;
    }
}
