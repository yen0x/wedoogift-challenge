package com.wedoogift.interview;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.interview.dao.FakeDatabase;
import com.wedoogift.interview.model.Company;
import com.wedoogift.interview.model.Distribution;
import com.wedoogift.interview.model.InputDTO;
import com.wedoogift.interview.model.User;
import com.wedoogift.interview.model.exception.EndowmentException;
import com.wedoogift.interview.service.EndowmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class App {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        var path = "data/input.json";
        var endowmentService = new EndowmentService();
        try {
            // reads input from file
            if (args.length > 2)
                path = args[1];

            // inits our fakedatabase with the file input
            var inputDTO = readInputFile(path);
            FakeDatabase.initDatabase(inputDTO);

            for (Distribution distribution : inputDTO.distributions) {
                try {
                    endowmentService.distributeGiftCard(distribution);
                } catch (EndowmentException e) {
                    LOGGER.error("Could not send gift card", e);
                }
            }

            var result = generateResult();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            System.exit(0);
        }
    }

    private static InputDTO readInputFile(String path) throws IOException {
        String json;
        try {
            json = Files.readAllLines(Path.of(path)).stream().collect(Collectors.joining());
        } catch (IOException e) {
            LOGGER.error("Could not read file at {}", path, e);
            throw e;
        }

        var objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, InputDTO.class);
    }

    private static InputDTO generateResult() {
        InputDTO result = new InputDTO();
        result.users = FakeDatabase.getUserList();
        result.companies = FakeDatabase.getCompanyList();
        return result;
    }
}
